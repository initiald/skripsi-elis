<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductCategoryController;
// member
use App\Http\Controllers\Member\HomeController;
// seles
use App\Http\Controllers\SalesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('pages.member.home');
    return redirect()->route('home');
});
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/home/{id}', [HomeController::class, 'show'])->name('detail');
Route::post('/home/cart', [HomeController::class, 'addCart'])->name('add_cart');
Route::post('/home', [HomeController::class, 'store'])->name('store');
Route::get('/home/{idx}/cart', [HomeController::class, 'delete_cart_item'])->name('delete_cart_item');
Route::get('/home/checkout/form', [HomeController::class, 'checkout'])->name('checkout');

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/starter', function (Request $request) {
    // $request->session()->put();
    // dd($request->session()->all());
    return view('layouts.app');
})->name('dashboard');

// Route::get('/home', 'App\Http\Controllers\DashboardController@index')->name('home');
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('auth');
Route::get('/member/register', [UserController::class, 'index'])->name('register');
// group
Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    // sales
    Route::get('/sales', [SalesController::class, 'index'])->name('sales');
    Route::put('/sales/{id}', [SalesController::class, 'update'])->name('sales_update');
    // products
    Route::get('/product', [ProductController::class, 'index'])->name('product');
    Route::put('/product/{id}', [ProductController::class, 'update'])->name('product_update');
    Route::post('/product', [ProductController::class, 'store'])->name('product_store');
    Route::get('/product/{id}/toggle', [ProductController::class, 'toggle_delete'])->name('product_toggle_delete');
    // product category
    Route::get('/product/category', [ProductCategoryController::class, 'index'])->name('product_category');
    Route::post('/product/category', [ProductCategoryController::class, 'store'])->name('product_category_store');
    Route::get('/product/category/{id}/toggle', [ProductCategoryController::class, 'toggle_delete'])->name('product_category_toggle_delete');
    Route::put('/product/category/{id}', [ProductCategoryController::class, 'update'])->name('product_category_update');
    Route::resources([
        // 'product/category' => ProductCategoryController::class,
        // 'salary' => AdminSalaryController::class,
    ]);
    // management users
    Route::get('/register/user', [UserController::class, 'admin_register_user'])->name('register-user');
    Route::post('/register/user', [UserController::class, 'store_register_user'])->name('store-register-user');
    Route::get('/operator', [UserController::class, 'admin_operator'])->name('operator');
    Route::put('/operator/{id}', [UserController::class, 'admin_operator_update'])->name('operator-update');
    Route::put('/member/{id}', [UserController::class, 'admin_member_update'])->name('member-update');
    Route::get('/member', [UserController::class, 'admin_member'])->name('member');
    Route::get('/user/{id}/toggle', [UserController::class, 'admin_user_toggle_delete'])->name('toggle-delete');
    // Route::get('/activate/{id}', [UserController::class, 'admin_user_activate'])->name('activate');

    Route::prefix('member')->group(function () {
        Route::get('/user', function () {// route('member/user')
            // Matches The "/admin/users" URL
        });
    });
});

Route::get('/flights', function () {
    // Only authenticated users may access this route...
})->middleware('auth');


