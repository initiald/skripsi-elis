<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('product_categories_id')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('basic_price')->nullable();
            $table->integer('sale_price')->nullable();
            $table->integer('sku')->nullable();
            $table->string('sku_status')->nullable();
            // $table->string('color_id')->nullable();
            // $table->string('size_id')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
