@push('name')
    
@endpush
<div class="modal fade" id="product-category-form-{{isset($id) ? $id:'0'}}">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{!isset($id)?__('title.form_add_new_product_category'):__('title.form_edit_product_category')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{!isset($id)?route('product_category_store'):route('product_category_update',$id)}}" method="post">
                @csrf
                @if(isset($id))
                    <input type="hidden" name="id" value="{{$id}}">
                    @method('PUT')
                @else
                    @method('POST')
                @endif
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control capitalize" placeholder="Name" value="{{isset($name)?$name:''}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fa fa-archive"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('button.close')}}</button>
                <button type="submit" class="btn btn-primary">{{__('button.save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
<script>
    
    // function save() {
    //     console.log('test clicked')
    //     let id = $("input[name=id]").val();
    //     let name = $("input[name=name]").val();
    //     let email = $("input[name=email]").val();
    //     let telp = $("input[name=telp]").val();
    //     let address = $("input[name=address]").val();
    //     let csrf = $("input[name=_token]").val();
    //     let url = $("input[name=url]").val();
    //     let data = {id:id,name:name,email:email,telp:telp,address:address,_token:csrf,_method:'PUT'};
    //     console.log(data)
    //     $.post(url,data, function(data, status){
    //         // alert("Data: " + data + "\nStatus: " + status);

    //         console.log(data)
    //         console.log(status)
    //     });
    // }
</script>
@endpush