<div class="modal fade" id="sales-detail-{{$id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('title.detail_sales')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <dl class="row">
                  <dt class="col-sm-4">{{__('table.name')}}</dt>
                  <dd class="col-sm-8 text-capitalize">{{$name}}</dd>
                  <dt class="col-sm-4">{{__('table.order_id')}}</dt>
                  <dd class="col-sm-8">{{$order_id}}</dd>
                  <dt class="col-sm-4">{{__('table.status')}}</dt>
                  <dd class="col-sm-8">{{__('status.'.$status)}}</dd>
                  <dt class="col-sm-4">{{__('table.total')}}</dt>
                  <dd class="col-sm-8">{{currencies($total)}}</dd>
                  <dt class="col-sm-4">{{__('table.shipping')}}</dt>
                  <dd class="col-sm-8">
                    <label class="text-capitalize">{{$shipping->name}}</label> ({{$shipping->telp}}) <br>
                    {{$shipping->address}}, {{$shipping->city}} - {{$shipping->province}}
                  </dd>
                </dl>
                {{-- <dl class="row">
                    dt
                </dl> --}}
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>{{__('table.name')}}</th>
                            <th>{{__('table.basic_price')}}</th>
                            <th>{{__('table.sale_price')}}</th>
                            <th>{{__('table.qty')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($details as $detail)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td class="text-capitalize">{{$detail->name_product}}</td>
                            <td>{{currencies($detail->basic_price)}}</td>
                            <td>{{currencies($detail->sale_price)}}</td>
                            <td>{{$detail->qty}}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>