<div class="modal fade" id="member-detail-{{$id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('title.detail_member')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <dl class="row">
                  <dt class="col-sm-4">{{__('table.name')}}</dt>
                  <dd class="col-sm-8">{{$name}}</dd>
                  <dt class="col-sm-4">{{__('table.email')}}</dt>
                  <dd class="col-sm-8">{{$email}}</dd>
                  {{-- <dd class="col-sm-8 offset-sm-4">Donec id elit non mi porta gravida at eget metus.</dd> --}}
                  <dt class="col-sm-4">{{__('table.telp')}}</dt>
                  <dd class="col-sm-8">{{$telp}}</dd>
                  <dt class="col-sm-4">{{__('table.address')}}</dt>
                  <dd class="col-sm-8">{{$address}}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>