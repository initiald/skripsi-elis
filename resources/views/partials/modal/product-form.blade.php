<div class="modal fade" id="product-form-{{isset($id) ? $id:'0'}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{!isset($id)?__('title.form_add_new_product'):__('title.form_edit_product')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{!isset($id)?route('product_store'):route('product_update',$id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                @if(isset($id))
                    <input type="hidden" name="id" value="{{$id}}">
                    @method('PUT')
                @else
                    @method('POST')
                @endif
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.name')}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" placeholder="Name" value="{{isset($name)?$name:''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.description')}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="description" class="form-control" placeholder="description" value="{{isset($description)?$description:''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.basic_price')}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="basic_price" class="form-control" placeholder="8888" value="{{isset($basic_price)?$basic_price:''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.sale_price')}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="sale_price" class="form-control" placeholder="9999" value="{{isset($sale_price)?$sale_price:''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.sku')}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="sku" class="form-control" placeholder="50" value="{{isset($sku)?$sku:''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.sku_status')}}</label>
                        <div class="col-sm-10">
                            <select name="sku_status" class="form-control">
                                <option value="in_stock">{{__('global.in_stock')}}</option>
                                <option value="out_of_stock">{{__('global.out_of_stock')}}</option>
                                <option value="on_backorder">{{__('global.on_backorder')}}</option>
                                <option value="indent">{{__('global.indent')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{__('table.image')}}</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control input-files" name="images[]" multiple>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('button.close')}}</button>
                <button type="submit" class="btn btn-primary">{{__('button.save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
<script>
    // function save() {
    //     console.log('test clicked')
    //     let id = $("input[name=id]").val();
    //     let name = $("input[name=name]").val();
    //     let description = $("input[name=description]").val();
    //     let basic_price = $("input[name=basic_price]").val();
    //     let sale_price = $("input[name=sale_price]").val();
    //     let csrf = $("input[name=_token]").val();
    //     let url = $("input[name=url]").val();
    //     let data = {id:id,name:name,description:description,basic_price:basic_price,sale_price:sale_price,_token:csrf,_method:'PUT'};
    //     console.log(data)
    //     $.post(url,data, function(data, status){
    //         // alert("Data: " + data + "\nStatus: " + status);

    //         console.log(data)
    //         console.log(status)
    //     });
    // }
</script>
@endpush