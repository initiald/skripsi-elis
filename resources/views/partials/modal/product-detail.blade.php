<div class="modal fade" id="product-detail-{{$id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('title.detail_product')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <dl class="row">
                  <dt class="col-sm-4">{{__('table.name')}}</dt>
                  <dd class="col-sm-8">{{$name}}</dd>
                  <dt class="col-sm-4">{{__('table.description')}}</dt>
                  <dd class="col-sm-8">{{$description}}</dd>
                  <dt class="col-sm-4">{{__('table.basic_price')}}</dt>
                  <dd class="col-sm-8">{{$basic_price}}</dd>
                  <dt class="col-sm-4">{{__('table.sale_price')}}</dt>
                  <dd class="col-sm-8">{{$sale_price}}</dd>
                  <dt class="col-sm-4">{{__('table.sku')}}</dt>
                  <dd class="col-sm-8">{{$sku}}</dd>
                  <dt class="col-sm-4">{{__('table.sku_status')}}</dt>
                  <dd class="col-sm-8">{{__('global.'.$sku_status)}}</dd>
                  <dt class="col-sm-4">{{__('table.image')}}</dt>
                  <dd class="col-sm-8"><img src="{{'uploads/'.$image}}" width="100px"></dd>
                </dl>
            </div>
        </div>
    </div>
</div>