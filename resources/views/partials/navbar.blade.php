<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      @foreach (config('adminlte.left_navbar') as $item)
        <li class="nav-item d-none d-sm-inline-block">
          @hasSection ('navbar_left')
            @if (isset($item['modal']))  
              <a class="nav-link {{getUri() != $item['slug'] ? 'hide':''}}" data-toggle="modal" data-target="#{{$item['data-target']}}">{{__('menu.'.$item['text'])}}</a>              
            @else
              <a href="{{$item['url']}}" class="nav-link {{getUri() != $item['slug'] ? 'hide':''}}">{{__('menu.'.$item['text'])}}</a>
            @endif            
          @endif
        </li>
      @endforeach
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item {{config('adminlte.right_navbar.search') ?:'hide'}}">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Cart Dropdown Menu -->
      <li class="nav-item dropdown {{config('adminlte.right_navbar.cart') ?:'hide'}}">
        @php
          $carts = null;
          if (session('carts') !== null){
            $carts = session('carts');
          }
        @endphp
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-shopping-cart"></i>
          <span class="badge badge-danger navbar-badge">{{$carts ? count($carts):0}}</span>
        </a>
        @if ($carts)
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            @foreach (session('carts') as $cart)
            <div class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="{{ '/uploads/'.$cart['image']}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    {{$cart['name']}}
                    <span class="float-right text-sm text-danger"><a href="{{route('delete_cart_item',$loop->index)}}"><i class="fa fa-trash"></i></a></span>
                  </h3>
                  <p class="text-sm">Rp. {{number_format($cart['price'],0,",",".")}}</p>
                  <p class="text-sm text-muted">{{$cart['qty']}} Item(s)</p>
                </div>
              </div>
              <!-- Message End -->
            </div>
            <div class="dropdown-divider"></div>
            @endforeach
            <a href="{{route('checkout')}}" class="dropdown-item dropdown-footer">{{__('button.checkout')}}</a>
          </div>
        @endif
      </li>
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown {{config('adminlte.right_navbar.notification_chat') ?:'hide'}}">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ asset("/bower_components/admin-lte/dist/img/user1-128x128.jpg") }}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ asset("/bower_components/admin-lte/dist/img/user8-128x128.jpg") }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ asset("/bower_components/admin-lte/dist/img/user3-128x128.jpg") }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown {{config('adminlte.right_navbar.notification') ?:'hide'}}">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <!-- Profile Dropdown Menu -->
      <li class="nav-item dropdown {{config('adminlte.right_navbar.profile') ?:'hide'}}">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i>
          {{-- <span class="badge badge-danger navbar-badge">3</span> --}}
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ asset("/bower_components/admin-lte/dist/img/user3-128x128.jpg") }}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  @auth
                  {{Auth::user()->name}}
                  @endauth

                  @guest
                  {{__('global.guest')}}
                  @endguest
                  {{-- <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span> --}}
                </h3>
                @auth
                <p class="text-sm">{{Auth::user()->roles}}</p>
                @endauth

                @guest
                <p class="text-sm">{{__('global.guest')}}</p>
                @endguest
                {{-- <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p> --}}
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          @auth
          <a href="{{route('logout')}}" class="dropdown-item dropdown-footer">{{__('button.logout')}}</a>
          @endauth

          @guest
          <a href="{{route('login')}}" class="dropdown-item dropdown-footer">{{__('button.login')}}</a>
          @endguest
        </div>
      </li>
      <!-- Fullscreen toggle -->
      <li class="nav-item {{config('adminlte.right_navbar.fullscreen') ?:'hide'}}">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <!-- Controll sidebar toggle -->
      <li class="nav-item {{config('adminlte.right_navbar.controll_sidebar') ?:'hide'}}">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
</nav>