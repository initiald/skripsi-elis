{{-- <div>
    Livewire work, click <span wire:click="increment">here!</span> 
        <h1>{{ $count }} {{$dt}}</h1>
</div> --}}
<div class="btn-group">
    <button type="button" class="btn btn-sm btn-outline-success" wire:click="decrement({{$index}})" ><i class="fa fa-minus"></i></button>
    <input type="integer" value="{{$qty}}" style="width:50px" class="text-center">
    <button type="button" class="btn btn-sm btn-outline-success" wire:click="increment({{$index}})" ><i class="fa fa-plus"></i></button>
</div>
