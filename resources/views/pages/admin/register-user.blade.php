@extends('layouts.app')
@section('page_title', __('menu.add_new'))
@section('content')
<div class="row">
    <div class="col-lg-12">
    @if ($errors->any())
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i>{{session('flash_warning')}}</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('flash_success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i>{{session('flash_success')}}</h5>
        </div>
    @endif
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">{{__('title.form_add_new')}}</h3>
            </div>
            <!-- /.card-header -->
            <form action="{{route('store-register-user')}}" method="post">
                <div class="card-body">
                    @method('POST')
                    @csrf
                    <div class="input-group mb-3">
                        <select name="roles" class="form-control">
                            <option value="admin">Admin</option>
                            <option value="member">Member</option>
                        </select>
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fa fa-cogs"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="telp" class="form-control" placeholder="Telp">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-phone-square"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="address" class="form-control" placeholder="Address">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-address-card"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmation Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" name="save" value="1" class="btn btn-primary">{{__('button.save')}}</button>
                    <button type="submit" name="save_more" value="1" class="btn btn-primary">{{__('button.save_more')}}</button>
                </div>
            </form>
            <!-- /.card-body -->
          </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    
    // function save() {
    //     console.log('test clicked')
    //     let id = $("input[name=id]").val();
    //     let name = $("input[name=name]").val();
    //     let email = $("input[name=email]").val();
    //     let telp = $("input[name=telp]").val();
    //     let address = $("input[name=address]").val();
    //     let csrf = $("input[name=_token]").val();
    //     let url = $("input[name=url]").val();
    //     let data = {id:id,name:name,email:email,telp:telp,address:address,_token:csrf,_method:'PUT'};
    //     console.log(data)
    //     $.post(url,data, function(data, status){
    //         // alert("Data: " + data + "\nStatus: " + status);

    //         console.log(data)
    //         console.log(status)
    //     });
    // }
</script>
@endpush