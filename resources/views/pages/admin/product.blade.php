@extends('layouts.app')
@isset($navbar_left)
    @section('navbar_left', $navbar_left)
@endisset
@section('page_title', __('menu.products'))
@section('content')
<div class="row">
    <div class="col-lg-12">
    @if ($errors->any())
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i>{{session('flash_warning')}}</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('flash_success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i>{{session('flash_success')}}</h5>
        </div>
    @endif
    </div>
</div>
<div class="row">
                
    {{-- {!!'<pre>'!!} --}}
    {{-- {{print_r($products)}} --}}
    {{-- {!!'</pre>'!!} --}}
    <div class="col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">{{__('title.list_product')}}</h3>

              <div class="card-tools">
                  {{$products->links('partials.pagination.adminlte')}}
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 h-scroll">
              <table class="table">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>{{__('table.name')}}</th>
                        <th>{{__('table.sku')}}</th>
                        <th>{{__('table.sku_status')}}</th>
                        <th>{{__('table.basic_price')}}</th>
                        <th>{{__('table.sale_price')}}</th>
                        <th>{{__('table.status')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->sku}}</td>
                        <td>{{__('global.'.$product->sku_status)}}</td>
                        <td>{{$product->basic_price}}</td>
                        <td>{{$product->sale_price}}</td>
                        <td><input type="checkbox" name="my-bs-status" data-url="{{route('product_toggle_delete',$product->id)}}" {{!$product->trashed() ? 'checked':''}} data-bootstrap-switch data-off-color="danger" data-on-color="success"></td>
                        <td>
                            <div class="btn-group" style="width:90px !important">
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#product-form-{{$product->id}}">{{__('button.edit')}}</button>
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#product-detail-{{$product->id}}">{{__('button.detail')}}</button>
                            </div>
                        </td>
                    </tr>
                    @include('partials.modal.product-form',['id'=>$product->id,'name'=>$product->name,'description'=>$product->description,'sku'=>$product->sku,'sku_status'=>$product->sku_status,'basic_price'=>$product->basic_price,'sale_price'=>$product->sale_price,'image'=>$product->image,'id_category'=>$product->product_categoes_id])
                    @include('partials.modal.product-detail',['id'=>$product->id,'name'=>$product->name,'description'=>$product->description,'sku'=>$product->sku,'sku_status'=>$product->sku_status,'basic_price'=>$product->basic_price,'sale_price'=>$product->sale_price,'image'=>$product->image,'id_category'=>$product->product_categoes_id])
                @endforeach
                @include('partials.modal.product-form')
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
<!-- /.col-md-6 -->
</div>
<!-- /.product -->
@endsection
@push('scripts')
<script>
    
    $(function () {
        $("[name='my-bs-status']").bootstrapSwitch({
            onSwitchChange: function(e, state) {
                // alert('change')
                let url = $(this).data('url');
                $.get(url, function (response) {
                    // console.log(response)
                    if (response.code == 200) {
                        $.each(response, function (i, obj) {
                        console.log(obj)
                            // $('#gaji_pokok').val(obj.gaji_pokok)
                        })
                    }
                },'json');
            }
        });
      $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      })
    })
</script>
@endpush