@extends('layouts.app')
@isset($navbar_left)
    @section('navbar_left', $navbar_left)
@endisset
@section('page_title', __('menu.sales'))
@section('content')
<div class="row">
    <div class="col-lg-12">
    @if ($errors->any())
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i>{{session('flash_warning')}}</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('flash_success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i>{{session('flash_success')}}</h5>
        </div>
    @endif
    </div>
</div>
<div class="row">
                
    {{-- {!!'<pre>'!!} --}}
    {{-- {{print_r($sales)}} --}}
    {{-- {!!'</pre>'!!} --}}
    <div class="col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">{{__('title.list_sale')}}</h3>

              <div class="card-tools">
                  {{$sales_ori->links('partials.pagination.adminlte')}}
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 h-scroll">
              <table class="table">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>{{__('table.name')}}</th>
                        <th>{{__('table.order_id')}}</th>
                        <th>{{__('table.total')}}</th>
                        <th>{{__('table.status')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($sales as $sale)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td class="text-capitalize">{{$sale->name != 'guest' ?$sale->name :__('global.guest')}}</td>
                        <td>{{$sale->order_id}}</td>
                        <td>{{currencies($sale->total_price)}}</td>
                        <td>
                            {{-- {{__('status.'.$sale->status)}} --}}
                            <select name="status-{{$sale->id}}" class="form-control update-status" data-update="{{$sale->id}}">
                                <option value="waiting_payment" {{$sale->status == 'waiting_payment' ? 'selected':''}}>{{__('status.waiting_payment')}}</option>
                                <option value="sent" {{$sale->status == 'sent' ? 'selected':''}}>{{__('status.sent')}}</option>
                                <option value="pending" {{$sale->status == 'pending' ? 'selected':''}}>{{__('status.pending')}}</option>
                                <option value="cancel" {{$sale->status == 'cancel' ? 'selected':''}}>{{__('status.cancel')}}</option>
                                <option value="on_process" {{$sale->status == 'on_process' ? 'selected':''}}>{{__('status.on_process')}}</option>
                                <option value="returned" {{$sale->status == 'returned' ? 'selected':''}}>{{__('status.returned')}}</option>
                                <option value="settled" {{$sale->status == 'settled' ? 'selected':''}}>{{__('status.settled')}}</option>
                            </select>
                        </td>
                        <td>
                            <div class="btn-group" style="width:90px !important">
                                {{-- <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#sales-form-{{$sale->id}}">{{__('button.update')}}</button> --}}
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#sales-detail-{{$sale->id}}">{{__('button.detail')}}</button>
                            </div>
                        </td>
                    </tr>
                    {{-- @include('partials.modal.sales-form',['id'=>$sale->id,'name'=>$sale->name,'description'=>$sale->description,'sku'=>$sale->sku,'sku_status'=>$sale->sku_status,'basic_price'=>$sale->basic_price,'sale_price'=>$sale->sale_price,'image'=>$sale->image,'id_category'=>$sale->sale_categoes_id]) --}}
                    {{-- @include('partials.modal.sales-detail',['id'=>$sale->id,'name'=>$sale->name,'order_id'=>$sale->order_id,'shipping'=>$sale->shipping,'status'=>$sale->status]) --}}
                @endforeach
                {{-- @include('partials.modal.sale-form') --}}
                </tbody>
              </table>
              @foreach ($sales as $sale)
                  @include('partials.modal.sales-detail',['id'=>$sale->id,'name'=>$sale->name,'order_id'=>$sale->order_id,'total'=>$sale->total_price,'shipping'=>$sale->shipping,'details'=>$sale->details,'status'=>$sale->status])
              @endforeach
            </div>
            <!-- /.card-body -->
          </div>
    </div>
<!-- /.col-md-6 -->
</div>
<!-- /.sale -->
{{-- <input type="hidden" name="url" value="{{route('sales_update')}}"> --}}
@endsection
@push('scripts')
<script>
    
    $(".update-status").change(function() {
        let id = $(this).data('update');
        // alert('hi '+id)
        let csrf = $("meta[name=csrf-token]").attr('content');
        let url = "/sales/"+id;
        let status = $("select[name=status-"+id+"]").val();
        let data = {id:id,status:status,_token:csrf,_method:'PUT'};
        // console.log(url)
        // console.log(data)
        $.post(url,data, function(data, status){
            // alert("Data: " + data + "\nStatus: " + status);
            // console.log(data)
            // console.log(status)
            if (data.code == 200) {
                alert('Update success.')
            }
        });
    })    
    // $(function () {
    // })
</script>
@endpush