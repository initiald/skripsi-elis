@extends('layouts.basic')
@section('page_title', 'Checkout')
@section('content')

<div class="row">
    <div class="col-lg-12">
    @if ($errors->any())
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i>{{session('flash_warning')}}</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('flash_success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i>{{session('flash_success')}}</h5>
        </div>
    @endif
    </div>
</div>
<div class="row">
    {{-- {{pre($carts)}} --}}
    {{-- {!!'<pre>'!!}
    {{print_r(config('adminlte.sidebar_menu')[4])}}
    {!!'</pre>'!!} --}}
    <div class="col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header">
            <h3 class="card-title">{{__('global.shipping_address')}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('store')}}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label >{{__('table.name')}}</label>
                        <input type="text" class="form-control" name="name" placeholder="{{__('table.name')}}">
                    </div>
                    <div class="form-group">
                        <label >{{__('table.telp')}}</label>
                        <input type="text" class="form-control" name="telp" placeholder="{{__('table.telp')}}">
                    </div>
                    <div class="form-group">
                        <label >{{__('table.province')}}</label>
                        <input type="text" class="form-control" name="province" placeholder="{{__('table.province')}}">
                    </div>
                    <div class="form-group">
                        <label >{{__('table.city')}}</label>
                        <input type="text" class="form-control" name="city" placeholder="{{__('table.city')}}">
                    </div>
                    <div class="form-group">
                        <label >{{__('table.address')}}</label>
                        <input type="text" class="form-control" name="address" placeholder="{{__('table.address')}}">
                    </div>
                    <div class="form-group">
                        <label >{{__('table.notes')}}</label>
                        <textarea name="notes" class="form-control" rows="5"></textarea>
                    </div>
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{__('button.save_pay')}}</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">{{__('global.summary')}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            {{-- <form> --}}
            <div class="card-body">
                @foreach ($carts as $cart)
                    <div class="media">
                        <img src="{{ '/uploads/'.$cart['image']}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                        <h3 class="dropdown-item-title">
                            {{$cart['name']}}
                            <span class="float-right text-sm text-danger"><a href="{{route('delete_cart_item',$loop->index)}}"><i class="fa fa-trash"></i></a></span>
                        </h3>
                        <span class="text-sm">Rp. {{number_format($cart['price'],0,",",".")}}</span>
                        <br>
                        {{-- <br> --}}
                        @livewire('cart-item-change',['qty' => $cart['qty'],'index'=>$loop->index])
                        <span class="text-sm text-muted"> {{__('global.items')}}</span>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                @endforeach
            </div>
            <!-- /.card-body -->

            {{-- <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div> --}}
            {{-- </form> --}}
        </div>
    </div>
<!-- /.col-md-6 -->
</div>
<!-- /.row -->
@endsection
@push('styles')
@endpush