@extends('layouts.basic')
@section('page_title', 'Home')
@section('content')

<div class="row">
    {{-- {{pre(session('carts'))}} --}}
    {{-- {!!'<pre>'!!}
    {{print_r(config('adminlte.sidebar_menu')[4])}}
    {!!'</pre>'!!} --}}
    @foreach ($products as $product)
    <div class="col-12 col-sm-4 col-md-3 d-flex align-items-stretch flex-column">
        <div class="card bg-light d-flex flex-fill card-primary card-outline">
            {{-- <div class="card-header text-muted border-bottom-0">
                Rp. 9.000.000
            </div> --}}
            <div class="card-body p-0">
                <img class="card-img-top" src="{{ '/uploads/'.$product->image}}" alt="Dist Photo 3">
                <div class="row p-3">
                    <h5 class="card-title text-primary col-12" style="font-weight: bold;">{{$product->name}}</h5>
                    <br>
                    <h5 class="card-title text-primary col-12">Rp. {{number_format($product->sale_price,0,",",".")}}</h5>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <div class="btn-group">
                        <form action="{{route('add_cart')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$product->id}}">
                            <input type="hidden" name="name" value="{{$product->name}}">
                            <input type="hidden" name="price" value="{{$product->sale_price}}">
                            <input type="hidden" name="image" value="{{$product->image}}">
                            <button type="submit" class="btn btn-sm btn-outline-success" >{{__('button.add_cart')}}</button>
                        </form>
                        <a href="{{route('detail',$product->id)}}" type="button" class="btn btn-sm btn-outline-success" >{{__('button.detail')}}</a>
                    </div>
                    {{-- @livewire('cart-badge') --}}
                </div>
            </div>
        </div>
    </div>
    @endforeach
<!-- /.col-md-6 -->
</div>
<!-- /.row -->
@endsection
@push('styles')
<style>
    .card-img-top {
        max-height: 240px;
    }    
</style>
@endpush