@extends('layouts.basic')
@section('page_title', __('menu.purchase_invoice'))
@section('content')

<div class="row">
    <div class="col-lg-12">
    @if ($errors->any())
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i>{{session('flash_warning')}}</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('flash_success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i>{{session('flash_success')}}</h5>
        </div>
    @endif
    <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-check"></i>{{__('global.thankyou_purchase')}}</h5>
    </div>
    </div>
</div>
<div class="row">
    {{-- {{pre(session('carts'))}} --}}
    {{-- {!!'<pre>'!!}
    {{print_r(config('adminlte.sidebar_menu')[4])}}
    {!!'</pre>'!!} --}}
    <div class="invoice p-3 mb-3">
        <!-- title row -->
        <div class="row">
          <div class="col-12">
            <h4>
              <i class="fas fa-globe"></i> {{ config('app.name') }}
              <small class="float-right">{{__('table.date')}} : {{date('d-m-Y',strtotime($sales->created_at))}}</small>
            </h4>
          </div>
          <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">
            {{__('global.from')}}
            <address>
              <strong>Admin</strong><br>
              795 Folsom Ave, Suite 600<br>
              San Francisco, CA 94107<br>
              Phone: (804) 123-5432<br>
              Email: info@pgds.my.id
            </address>
          </div>
          <!-- /.col -->
          <div class="col-sm-4 invoice-col">
            {{__('global.to')}}
            <address>
              <strong>{{$shippings->name}}</strong><br>
              {{$shippings->address}}, {{$shippings->city}}, {{$shippings->province}} <br>
              {{__('table.telp')}}: (555) 539-1037<br>
              {{-- Email: john.doe@example.com --}}
            </address>
          </div>
          <!-- /.col -->
          <div class="col-sm-4 invoice-col">
            <b class="float-right">{{__('global.order_id')}} : {{$sales->order_code}}</b><br>
            {{-- <br> --}}
            {{-- <b>{{__('global.order_id')}}:</b> {{$sales->order_code}}<br> --}}
            {{-- <b>Payment Due:</b> 2/22/2014<br> --}}
            {{-- <b>{{__('global.account')}}:</b> $sales->users_id --}}
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
          <div class="col-12 table-responsive">
              {{-- {{pre($details)}} --}}
            <table class="table table-striped">
              <thead>
              <tr>
                <th>{{__('table.qty')}}</th>
                <th>{{__('table.name')}}</th>
                <th>{{__('table.price')}}</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($details as $detail)
                <tr>
                  <td>{{$detail['qty']}}</td>
                  <td>{{$detail['products_id']}}</td>
                  <td>Rp. {{number_format($detail['sale_price'],0,",",".")}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <!-- accepted payments column -->
          <div class="col-6">
            <p class="lead">Payment Methods:</p>
            <img src="{{ asset("/bower_components/admin-lte/dist/img/credit/visa.png") }}" alt="Visa">
            <img src="{{ asset("/bower_components/admin-lte/dist/img/credit/mastercard.png") }}" alt="Mastercard">
            <img src="{{ asset("/bower_components/admin-lte/dist/img/credit/american-express.png") }}" alt="American Express">
            <img src="{{ asset("/bower_components/admin-lte/dist/img/credit/paypal2.png") }}" alt="Paypal">
          </div>
          <!-- /.col -->
          <div class="col-6">
            {{-- <p class="lead">Amount Due 2/22/2014</p> --}}

            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%">{{__('table.total')}}:</th>
                  <td>{{number_format($sales->total_price,0,",",".")}}</td>
                </tr>
                {{-- <tr>
                  <th>Tax (9.3%)</th>
                  <td>$10.34</td>
                </tr>
                <tr>
                  <th>Shipping:</th>
                  <td>$5.80</td>
                </tr>
                <tr>
                  <th>{{__('table.total')}}:</th>
                  <td>$265.24</td>
                </tr> --}}
              </table>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
          <div class="col-12">
            {{-- <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a> --}}
            <a href="https://api.whatsapp.com/send?phone=0817550189&text=payment confirmation order id {{$sales->order_code}}" target="_blank" type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> {{__('button.payment_confirmation')}}
            </a>
            <button type="button" class="btn btn-primary float-right" onclick = "print()" style="margin-right: 5px;">
              <i class="fas fa-download"></i> Print & Download
            </button>
          </div>
        </div>
    </div>
<!-- /.col-md-6 -->
</div>
<!-- /.row -->
@endsection
@push('script')
<script>
function print() {
    window.print()
}
</script>
@endpush