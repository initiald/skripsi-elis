<?php

return [

    'edit'               => 'Edit',
    'detail'               => 'Detail',
    'view_detail'               => 'View Detail',
    'add'               => 'Add',
    'add_new'               => 'Add New',
    'add_cart'               => 'Add to Cart',
    'save'               => 'Save',
    'save_pay'               => 'Save & Pay',
    'save_more'           => 'Save & Add More',
    'close'               => 'Close',
    'logout'               => 'Logout',
    'login'               => 'Login',
    'checkout'               => 'Checkout',
    'update'               => 'Update',
    'payment_confirmation'               => 'Payment Confirmation',
];
