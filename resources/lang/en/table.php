<?php

return [

    'name'               => 'Name',
    'email'               => 'Email',
    'telp'               => 'Telp',
    'province'               => 'Province',
    'city'               => 'city',
    'address'               => 'Address',
    'status'               => 'Status',
    'sku'               => 'SKU',
    'sku_status'               => 'SKU Status',
    'basic_price'               => 'Basic Price',
    'sale_price'               => 'Sale Price',
    'description'               => 'Description',
    'image'               => 'Image',
    'notes'               => 'Notes',
    'date'               => 'Date',
    'price'               => 'Price',
    'qty'               => 'Qty',
    'subtotal'               => 'Subtotal',
    'total'               => 'Total',
    'order_id'               => 'Order ID',
    'shipping'               => 'Shipping',
];
