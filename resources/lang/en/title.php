<?php

return [

    'edit_operator'               => 'Edit Operator',
    'list_operator'               => 'List Operator',
    'detail_operator'             => 'Detail Operator',
    'list_product'               => 'List Product',
    'list_sale'               => 'List Sales',
    'detail_sales'             => 'Detail Sales',
    'detail_product'             => 'Detail Product',
    'edit_member'               => 'Edit Member',
    'edit_product'               => 'Edit Product',
    'list_member'               => 'List Member',
    'list_product_category'               => 'List Product Category',
    'detail_member'             => 'Detail Member',
    'form_add_new'                => 'Form add new user',
    'form_add_new_product'                => 'Form add new product',
    'form_edit_product'                => 'Form edit product',
    'form_add_new_product_category'                => 'Form add new product category',
    'form_edit_product_category'                => 'Form edit product category',
];
