<?php

return [

    'in_stock'               => 'In Stock',
    'out_of_stock'               => 'Out of Stock',
    'on_backorder'               => 'On Backorder',
    'indent'               => 'Indent',
    'guest'               => 'Guest',
    'shipping_address'               => 'Shipping Address',
    'summary'               => 'Summary',
    'items'               => 'Item(s)',
    'invoice_code'               => 'Invoice Code',
    'from'               => 'From',
    'to'               => 'To',
    'invoice'               => 'Invoice',
    'order_id'               => 'Order ID',
    'account'               => 'Account',
    'thankyou_purchase'               => 'Thank you for making a purchase.
    Please immediately complete the payment and confirm the payment',
];
