<?php
return [
    'waiting_payment' => 'Waiting Payment',
    'sent' => 'Sent',
    'pending' => 'Pending',
    'cancel' => 'Cancel',
    'on_process' => 'On Process',
    'returned' => 'Returned',
    'settled' => 'Settled',
];