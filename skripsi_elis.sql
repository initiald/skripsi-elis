-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 04, 2021 at 08:25 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_elis`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `products_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2021_10_05_043254_act_users_1', 2),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 3),
(7, '2021_10_05_133904_act_users_2', 3),
(8, '2021_10_12_070423_create_product_categories_table', 4),
(9, '2021_10_17_060117_create_products_table', 5),
(10, '2021_10_20_041739_create_images_table', 6),
(15, '2021_10_27_061942_create_shipping_table', 8),
(18, '2021_10_28_090256_create_sales_detail_table', 10),
(19, '2021_10_28_062643_create_sales_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_categories_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_price` int(11) DEFAULT NULL,
  `sale_price` int(11) DEFAULT NULL,
  `sku` int(11) DEFAULT NULL,
  `sku_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_categories_id`, `name`, `description`, `basic_price`, `sale_price`, `sku`, `sku_status`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Kebaya', 'Kebaya desu', 50001, 100001, 501, 'in_stock', '038c5efdb7648d0dbdcc5711c5c3a785.png', '2021-10-16 16:00:00', '2021-10-22 11:23:23', NULL),
(2, NULL, 'Sarung', 'sarung desu', 7000, 9000, 30, 'in_stock', '4b1e481f43afa371b9e9a693a7c8a3ae.png', '2021-10-17 08:34:24', '2021-10-22 11:43:01', NULL),
(3, NULL, 'alpukat', 'test desc', 8888, 9999, 50, 'out_of_stock', '7410a3b0befe4cd147ff0d551af67685.png', '2021-10-17 08:36:48', '2021-10-19 20:58:30', '2021-10-31 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'baju', '2021-10-12 16:00:00', NULL, NULL),
(2, 'celana', '2021-10-12 16:00:00', NULL, NULL),
(3, 'topi', '2021-10-13 01:04:39', '2021-10-13 01:04:39', NULL),
(4, 'Senjata', '2021-10-14 09:00:16', '2021-10-14 09:00:16', NULL),
(5, 'test', '2021-10-14 09:25:37', '2021-10-14 10:46:13', '2021-10-14 10:46:03'),
(6, 'test 2', '2021-10-14 10:06:41', '2021-10-14 10:06:41', NULL),
(7, 'test 33', '2021-10-14 10:23:55', '2021-10-14 10:39:56', '2021-10-14 10:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `shippings_id` int(11) DEFAULT NULL,
  `order_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `users_id`, `shippings_id`, `order_code`, `total_price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(74, NULL, 81, 'G9JTTWG586', 100001, 'waiting_payment', '2021-10-29 22:08:46', '2021-11-03 21:40:11', NULL),
(75, 11, 82, 'GAATCXN598', 109001, 'pending', '2021-10-29 22:09:49', '2021-11-03 21:42:02', NULL),
(76, 12, 83, 'GZEDCS8503', 218002, 'waiting_payment', '2021-10-29 22:10:25', '2021-10-29 22:10:25', NULL),
(77, 7, 84, 'GTKGKNF861', 500005, 'settled', '2021-10-30 01:44:43', '2021-11-03 21:45:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE `sales_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_id` int(11) DEFAULT NULL,
  `basic_price` int(11) DEFAULT NULL,
  `sale_price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_details`
--

INSERT INTO `sales_details` (`id`, `order_code`, `products_id`, `basic_price`, `sale_price`, `qty`, `created_at`, `updated_at`, `deleted_at`) VALUES
(76, 'G9JTTWG586', 1, 50001, 100001, 1, '2021-10-29 22:08:46', '2021-10-29 22:08:46', NULL),
(77, 'GAATCXN598', 1, 50001, 100001, 1, '2021-10-29 22:09:49', '2021-10-29 22:09:49', NULL),
(78, 'GAATCXN598', 2, 7000, 9000, 1, '2021-10-29 22:09:49', '2021-10-29 22:09:49', NULL),
(79, 'GZEDCS8503', 1, 50001, 100001, 2, '2021-10-29 22:10:25', '2021-10-29 22:10:25', NULL),
(80, 'GZEDCS8503', 2, 7000, 9000, 2, '2021-10-29 22:10:25', '2021-10-29 22:10:25', NULL),
(81, 'GTKGKNF861', 3, 50001, 100001, 5, '2021-10-30 01:44:43', '2021-10-30 01:44:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `users_id`, `name`, `telp`, `province`, `city`, `address`, `notes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(81, NULL, 'danang 81', '0817550189', 'Bali', 'Denpasar', 'Jln. Pulau Saelus II, Gg. Anggrek No. 2', NULL, '2021-10-29 22:08:46', '2021-10-29 22:08:46', NULL),
(82, NULL, 'danang 82', '0817550189', 'Bali', 'Denpasar', 'Jln. Pulau Saelus II, Gg. Anggrek No. 2', NULL, '2021-10-29 22:09:49', '2021-10-29 22:09:49', NULL),
(83, NULL, 'danang 84', '0817550189', 'Bali', 'Denpasar', 'Jln. Pulau Saelus II, Gg. Anggrek No. 2', NULL, '2021-10-29 22:10:25', '2021-10-29 22:10:25', NULL),
(84, 7, 'member', '0817550189', 'Bali', 'Denpasar', 'Address', 'notes', '2021-10-30 01:44:43', '2021-10-30 01:44:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `telp`, `address`, `roles`, `deleted_at`) VALUES
(3, 'Javck', 'admin@pgds.my.id', NULL, '$2y$10$KulfE3qW8PBKSwnA7DDuruLduzbRrjzHBVwB7jq06uNZPF5I.5ne2', NULL, '2021-09-30 16:00:00', '2021-10-17 08:39:02', '0817550189', 'Denpasar', 'administrator', NULL),
(5, 'Admin1', 'admin1@pgds.my.id', NULL, '$2y$10$/fzxJxImUfjJYTBBzhCY5OAQ/sm28HLeArAZqvFBbsMlODfpra8Ca', NULL, '2021-10-01 15:00:00', '2021-10-14 10:14:08', '0817550189', 'Denpasar', 'admin', '2021-10-14 10:14:08'),
(6, 'Admin123', 'admin123@pgds.my.id', NULL, '$2y$10$ZnRaud5XjEAKMNPJqAnCbukAkQ4DePylsMD56O5d4Za5Lw5Q8GcvC', NULL, '2021-10-05 16:00:00', '2021-10-06 10:07:53', '0817550189', 'Denpasar', 'admin', '2021-10-06 10:07:53'),
(7, 'member', 'member@pgds.my.id', NULL, '$2y$10$6aQM.paiOyFYYTUk8WeJC.xogpt4SWcLRtpdGMJhgsVypqSHAcddy', NULL, '2021-10-05 16:00:00', '2021-10-14 10:24:06', '0817550189', 'Badung', 'member', NULL),
(8, 'admin2', 'admin2@pgds.my.id', NULL, '$2y$10$ChZK5CNAiz0ICc/z/SqQHOZht/E2F.fdsTVCsE26KPUz5sgdqJx62', NULL, '2021-10-06 09:46:28', '2021-10-06 09:46:28', '08134578', 'Denpasar', 'admin', NULL),
(9, 'admin3', 'admin3@pgds.my.id', NULL, '$2y$10$8q5vAW6QIsURKvSJZjNkXe0zQqQavqs9YrAIr2CDtCmR6dgUwXFWG', NULL, '2021-10-06 09:47:11', '2021-10-14 10:26:23', '08134578', 'Badung', 'admin', '2021-10-14 10:26:23'),
(10, 'admin4', 'admin4@pgds.my.id', NULL, '$2y$10$/WS1Kb7rpIPb6dyoU6O71u8bfXtK3l.YuS2.QAWemBzX34m1qBqnm', NULL, '2021-10-06 09:47:36', '2021-10-06 09:47:36', '08134578', 'Kuta', 'admin', NULL),
(11, 'member1', 'member1@pgds.my.id', NULL, '$2y$10$vKhFSaLDLYo9kyvG8P8Y5uLtzMqGGZUaHBv72gqGE049f2fYe2NXq', NULL, '2021-10-06 10:09:07', '2021-10-06 10:35:06', '081345789', 'Gianyar', 'member', NULL),
(12, 'member2', 'member2@pgds.my.id', NULL, '$2y$10$EJogK0FrlFLlx/M1/lnxPeDsfjLnOraaq1eQP4oAEroE8fq8y4Opm', NULL, '2021-10-06 10:09:37', '2021-10-06 10:36:16', '08134578', 'Bali', 'member', '2021-10-06 10:36:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_details`
--
ALTER TABLE `sales_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `sales_details`
--
ALTER TABLE `sales_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
