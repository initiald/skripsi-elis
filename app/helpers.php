<?php

if (! function_exists('getUri')) {
    function getUri() {
        // $explode_uri = explode('?',$request->route()->uri());
        $explode_uri = explode('?',$_SERVER['REQUEST_URI']);
        return $explode_uri[0];
    }
}
if (! function_exists('pre')) {
    function pre($value) {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
    }
}
if (! function_exists('currencies')) {
    function currencies($value) {
        return number_format($value,0,",",".");
    }
}