<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;

class CartItemChange extends Component
{
    public $qty = 0;
    public $index = 0;
 
    public function decrement(Request $request, $idx)
    {
        if ($this->qty > 0) {
            $this->qty--;
            $carts = session('carts');
            $carts[$idx]['qty'] = $this->qty;
            $request->session()->put('carts', $carts);
        }
    }
    public function increment(Request $request, $idx)
    {
        $this->qty++;
        $carts = session('carts');
        $carts[$idx]['qty'] = $this->qty;
        $request->session()->put('carts', $carts);
    }

    public function render()
    {
        return view('livewire.cart-item-change',['dt'=>'danang']);
    }
}
