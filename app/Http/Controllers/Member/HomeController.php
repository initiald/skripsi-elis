<?php

namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\ProductCategories;
use App\Models\Products;
use App\Models\Images;
use App\Models\Shippings;
use App\Models\Sales;
use App\Models\SalesDetails;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // $request->session()->forget('carts');
        // pre(session('carts'));
        // $data['navbar_left'] = true;
        // $data['sub_menu'] = ['operators'];//,'recent','add_new'
        // $data['users'] = User::all();
        $data['products'] = Products::all();
        return view('pages.member.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prefix = Auth::user() ? 'M':'G';
        $carts = session('carts');
        $order_code = Str::upper($prefix.Str::random(6).rand(100,999));
        // dd($request);
        // dd($carts);
        if ($carts != null){
            $shipping = new Shippings();
            $shipping->users_id = Auth::id() ?: NULL;
            $shipping->name = $request->name;
            $shipping->telp = $request->telp;
            $shipping->province = $request->province;
            $shipping->city = $request->city;
            $shipping->address = $request->address;
            $shipping->notes = $request->notes;
            $shipping->save();
    
            $sale = new Sales();
            $sale->users_id = Auth::id() ?: NULL;
            $sale->shippings_id = $shipping->id;
            $sale->order_code = $order_code;
            $sale->total_price = 0;
            $sale->status = 'waiting_payment';
            $sale->save();
    
            if ($sale) {
                $total_price = 0;
                foreach ($carts as $key => $value) {
                    $product = Products::find($value['id']);
            
                    $sale_details = new SalesDetails();
                    $sale_details->products_id = $product->id;
                    $sale_details->order_code = $order_code;
                    $sale_details->basic_price = $product->basic_price;
                    $sale_details->sale_price = $product->sale_price;
                    $sale_details->qty = $value['qty'];
    
                    $total_price += $product->sale_price * $value['qty'];
                    $sale_details->save();
                }
                $update_sale = Sales::find($sale->id);
                $update_sale->total_price = $total_price;
                $update_sale->save();
            }
            // dd($sale_details);
            // bisa menggunakan redirect route atau back contoh penggunaan di proses update
            $save_shipping = $shipping ? true:false;
            $save_carts = $sale_details ? true:false;
    
            if ($save_carts & $save_shipping) {
                $request->session()->forget('carts');
                // dd('masuk');
                $dt['carts'] = $carts;
                $dt['total'] = $total_price;
                $dt['order_code'] = $order_code;
                return $this->invoice($shipping->id, $sale->id, $sale_details->id);
                // return redirect()->back()->with('flash_success','Thank you for making a purchase');
            }
        }

        return redirect()->back()->withErrors('flash_warning','Sorry, your purchase can not process.');
    }

    public function invoice($id_shipping, $id_sales, $order_code)
    {
        $data['sales'] = Sales::find($id_sales);
        $data['shippings'] = Shippings::find($id_shipping);
        $data['details'] = SalesDetails::where('order_code',$data['sales']['order_code'])->get();
        // $data['products'] = Products::find($data['details']['products_id']);
        // dd($data);
        return view('pages.member.invoice',$data);
    }

    public function addCart(Request $request)
    {
        $sess = [
            'id'=>$request->id,
            'name'=>$request->name,
            'price'=>$request->price,
            'image'=>$request->image,
            'qty'=>1,
        ];
        // $request->session()->forget('carts');
        if ($request->session()->exists('carts')) {
            // dd('exists');
            $get_sess = $request->session()->get('carts');
            // dd($get_sess);
            $request->session()->push('carts', $sess);
        }else{
            // dd('empty');
            $request->session()->put('carts', [$sess]);
        }
        // bisa menggunakan redirect route atau back contoh penggunaan di proses update
        return redirect()->back()->with('flash_success','Congratulatin, create product success.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['product'] = Products::find($id);
        return view('pages.member.home-detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    public function checkout(Request $request)
    {
        $data['carts'] = session('carts');
        if ($data['carts'] != null) {
            return view('pages.member.checkout',$data);
        }
        return redirect()->back()->with('flash_success','Congratulatin, create product success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function toggle_delete($id)
    {
    }

    public function delete_cart_item(Request $request, $idx)
    {
        $carts = session('carts');
        unset($carts[$idx]);
        // dd($carts);
        $request->session()->forget('carts');
        // rearrange array index
        foreach ($carts as $key => $value) {
            if ($request->session()->exists('carts')) {
                $get_sess = $request->session()->get('carts');
                $request->session()->push('carts', $carts[$key]);
            }else{
                $request->session()->put('carts', [$carts[$key]]);
            }
        }
        // foreach ($ray as &$item)
        return redirect()->back();
    }
}
