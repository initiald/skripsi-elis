<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategories;
use App\Models\Products;
use App\Models\Images;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['navbar_left'] = true;
        $data['sub_menu'] = ['operators'];//,'recent','add_new'
        // $data['users'] = User::all();
        $data['products'] = Products::withTrashed()
            // ->whereIn('roles',['administrator','admin'])
            ->paginate(5);// show all data with trashed / softdelete and pagination
        return view('pages.admin.product',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('images');
        // dd($request);
        $data = new Products();
        $data->name = $request->name;
        $data->description = $request->description;
        $data->basic_price = $request->basic_price;
        $data->sale_price = $request->sale_price;
        $data->sku = $request->sku;
        $data->sku_status = $request->sku_status;
        if (isset($file)) {
            $data->image = md5(time().$file[0]->getClientOriginalName()).'.'.$file[0]->getClientOriginalExtension();
            // upload document
            // make sure each file is valid
            if ($file[0]->isValid()) {
                // move the file from tmp to the destination path
                $file[0]->move(public_path('/uploads'), md5(time().$file[0]->getClientOriginalName()).'.'.$file[0]->getClientOriginalExtension());
            }
        }
        $data->save();

        // bisa menggunakan redirect route atau back contoh penggunaan di proses update
        if ($data) {
            return redirect()->back()->with('flash_success','Congratulatin, create product success.');
        }
        return redirect()->back()->withErrors('flash_warning','Create new product failed.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('images');

        $data = Products::withTrashed()->find($id);
        $data->name = $request->name;
        $data->description = $request->description;
        $data->basic_price = $request->basic_price;
        $data->sale_price = $request->sale_price;
        $data->sku = $request->sku;
        $data->sku_status = $request->sku_status;
        if (isset($file)) {
            $data->image = md5(time().$file[0]->getClientOriginalName()).'.'.$file[0]->getClientOriginalExtension();
            // upload document
            // foreach ($file as $fileKey => $fileObject ){
                // make sure each file is valid
                if ($file[0]->isValid()) {
                    // move the file from tmp to the destination path
                    $file[0]->move(public_path('/uploads'), md5(time().$file[0]->getClientOriginalName()).'.'.$file[0]->getClientOriginalExtension());
                }
            // }
        }
        $data->save();
        // dd($request);
        // pakai redirect back utk mendirect ke page / pagination sebelumnya dan form menggunakan modal jika tidak menggunakan modal akan di direct ke form kembali
        if ($data) {
            return redirect()->back()->with('flash_success','Congratulatin, update data success.');
        }
        return redirect()->back()->withErrors('flash_warning','update data failed.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function toggle_delete($id)
    {
        $data = Products::onlyTrashed()->where('id',$id);
        $res = ['code'=>400,'status'=>'failed'];
        if ($data->get()->count() > 0) {
            if($data->restore())
            $res = ['code'=>200,'status'=>'restore success'];
        }else{
            $data = Products::find($id);
            if($data->delete())
            $res = ['code'=>200,'status'=>'delete success'];
        }
        return response()->json($res);
    }
}
