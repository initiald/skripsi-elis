<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategories;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = ProductCategories::withTrashed()
            // ->whereIn('roles',['member'])
            ->paginate(5);// show all data with trashed / softdelete and pagination
        return view('pages.admin.product-category',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $store = new ProductCategories();
        $store->name = $request->name;
        $store->save();

        // bisa menggunakan redirect route atau back contoh penggunaan di proses update
        if ($store) {
            return redirect()->route('product_category')->with('flash_success','Congratulatin, create data success.');
        }
        return redirect()->route('product_category')->withErrors('flash_warning','Create new data failed.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ProductCategories::withTrashed()->find($id);
        $data->name = $request->name;
        $data->save();

        // pakai redirect back utk mendirect ke page / pagination sebelumnya dan form menggunakan modal jika tidak menggunakan modal akan di direct ke form kembali
        if ($data) {
            return redirect()->back()->with('flash_success','Congratulatin, update data success.');
        }
        return redirect()->back()->withErrors('flash_warning','update data failed.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function toggle_delete($id)
    {
        $data = ProductCategories::onlyTrashed()->where('id',$id);
        $res = ['code'=>400,'status'=>'failed'];
        if ($data->get()->count() > 0) {
            if($data->restore())
            $res = ['code'=>200,'status'=>'restore success'];
        }else{
            $data = ProductCategories::find($id);
            if($data->delete())
            $res = ['code'=>200,'status'=>'delete success'];
        }
        return response()->json($res);
    }
}
