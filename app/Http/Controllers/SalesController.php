<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategories;
use App\Models\Products;
use App\Models\Images;
use App\Models\Sales;
use App\Models\User;
use App\Models\Shippings;
use App\Models\SalesDetails;

class SalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $data['navbar_left'] = true;
        // $data['sub_menu'] = ['sales'];//,'recent','add_new'
        // $data['users'] = User::all();
        // $tes = User::find(7)->shippings;
        $sales = Sales::withTrashed()->paginate(2);
        // return;
        $arr_sales = [];
        // dd(User::withTrashed()->find(12)->name);
        foreach ($sales as $key => $sale) {
            // $get_products = Products::find($sale->shippings_id);
            // $get_shippings = Shipping::find($sale->shippings_id);
            // foreach ($get_shippings as $key => $value) {
                # code...
            // }
            $product = SalesDetails::addSelect(['name_product'=>Products::select('name')
                ->whereColumn('products_id', 'products.id')
                ->withTrashed()
            ])->where('order_code',$sale->order_code)->get();
            $tmp = (object) [
                'id' => $sale->id,
                'name' => $sale->users_id ? User::withTrashed()->find($sale->users_id)->name:'guest',
                'shipping' => Shippings::find($sale->shippings_id),
                // 'products' => $product,//where('order_code',$sale->order_code)->get()->products,
                // 'details' => SalesDetails::where('order_code',$sale->order_code)->get(),
                'details'=>$product,
                'order_id' => $sale->order_code,
                'total_price' => $sale->total_price,
                'status' => $sale->status,
            ];
            array_push($arr_sales,$tmp);
        }
        // dd($arr_sales);
        // return;
        $data['sales'] = $arr_sales;
        $data['sales_ori'] = $sales;
            // ->groupBy('')
            // ->whereIn('roles',['administrator','admin'])
            // ->paginate(5);// show all data with trashed / softdelete and pagination
        // $data['users'] = Users::all();
        return view('pages.admin.sales',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Sales::find($id);
        $data->status = $request->status;
        $data->save();
        // dd($request);
        // pakai redirect back utk mendirect ke page / pagination sebelumnya dan form menggunakan modal jika tidak menggunakan modal akan di direct ke form kembali
        if ($data) {
            return response()->json(['code'=>200,'msg'=>'Congratulatin, update data success.']);
        }
        return response()->json(['code'=>400,'msg'=>'Sorry, update data failed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function toggle_delete($id)
    {
        // $data = Products::onlyTrashed()->where('id',$id);
        // $res = ['code'=>400,'status'=>'failed'];
        // if ($data->get()->count() > 0) {
        //     if($data->restore())
        //     $res = ['code'=>200,'status'=>'restore success'];
        // }else{
        //     $data = Products::find($id);
        //     if($data->delete())
        //     $res = ['code'=>200,'status'=>'delete success'];
        // }
        // return response()->json($res);
    }
}
